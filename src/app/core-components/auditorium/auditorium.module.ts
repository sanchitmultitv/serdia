import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuditoriumRoutingModule } from './auditorium-routing.module';
import { AuditoriumComponent } from './auditorium.component';
import { LeftAuditoriumComponent } from './components/left-auditorium/left-auditorium.component';
import { RightAuditoriumComponent } from './components/right-auditorium/right-auditorium.component';
import { KbcQuizComponent } from './components/kbc-quiz/kbc-quiz.component';
import { VgCoreModule } from 'videogular2/compiled/core';
import { VgControlsModule } from 'videogular2/compiled/controls';
import { VgOverlayPlayModule } from 'videogular2/compiled/overlay-play';
import { VgBufferingModule} from 'videogular2/compiled/buffering';
import { VgStreamingModule } from 'videogular2/compiled/streaming';
import { FrontAuditoriumComponent } from './components/front-auditorium/front-auditorium.component';
import { FrontAuditoriumDeskComponent } from './components/front-auditorium-desk/front-auditorium-desk.component';

@NgModule({
  declarations: [AuditoriumComponent, LeftAuditoriumComponent, RightAuditoriumComponent, KbcQuizComponent, FrontAuditoriumComponent, FrontAuditoriumDeskComponent,],
  imports: [
    CommonModule,
    AuditoriumRoutingModule,
    VgCoreModule,
    VgControlsModule,
    VgBufferingModule,
    VgOverlayPlayModule,
    VgStreamingModule
  ]
})
export class AuditoriumModule { }
