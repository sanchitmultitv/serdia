import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontAuditoriumComponent } from './front-auditorium.component';

describe('FrontAuditoriumComponent', () => {
  let component: FrontAuditoriumComponent;
  let fixture: ComponentFixture<FrontAuditoriumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrontAuditoriumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontAuditoriumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
