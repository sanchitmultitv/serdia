import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-front-auditorium',
  templateUrl: './front-auditorium.component.html',
  styleUrls: ['./front-auditorium.component.scss']
})
export class FrontAuditoriumComponent implements OnInit {

  videoEnd = false;
  videoPlayer = 'https://dhzjgfv9krlhb.cloudfront.net/abr/smil:stream3.smil/playlist.m3u8';
  
  constructor() { }

  ngOnInit(): void {
  }
  videoEnded() {
    this.videoEnd = true;
  }
  playAudioClap(){
    let playaudio : any= document.getElementById('myAudioClap');
    playaudio.play();
  }
  playAudioWhistle(){
    let playaudio : any= document.getElementById('myAudioWhistle');
    playaudio.play();
  }
}
