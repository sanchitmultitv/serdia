import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-right-auditorium',
  templateUrl: './right-auditorium.component.html',
  styleUrls: ['./right-auditorium.component.scss']
})
export class RightAuditoriumComponent implements OnInit {
  videoEnd = false;
  videoPlayer = 'https://dhzjgfv9krlhb.cloudfront.net/abr/smil:stream3.smil/playlist.m3u8';
  //videoPlayer = '../assets/video/networking_lounge_video.mp4';
  constructor() { }

  ngOnInit(): void {
  }
  videoEnded() {
    this.videoEnd = true;
  }
  playAudioClap(){
    let playaudio : any= document.getElementById('myAudioClap');
    playaudio.play();
  }
}
