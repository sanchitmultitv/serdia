import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-front-auditorium-desk',
  templateUrl: './front-auditorium-desk.component.html',
  styleUrls: ['./front-auditorium-desk.component.scss']
})
export class FrontAuditoriumDeskComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }
  gotoAuditoriumFront(){
    this.router.navigate(['/auditorium/front-auditorium']);
  }
}
