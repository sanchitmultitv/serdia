import { Component, OnInit} from '@angular/core';
import { fadeAnimation } from '../../../../shared/animation/fade.animation';
import { FormGroup, FormControl, Validators, FormArray} from '@angular/forms';
import {FetchDataService} from '../../../../services/fetch-data.service'

import { EventEmitter } from 'events';
declare var $: any;
@Component({
  selector: 'app-left-auditorium',
  templateUrl:'./left-auditorium.component.html',
  styleUrls: ['./left-auditorium.component.scss'],
  animations: [fadeAnimation]
})
export class LeftAuditoriumComponent implements OnInit {
  ratingFormne = new FormGroup({
    
    password: new FormControl('', [Validators.required]),
   });
   ratingListFed:any=[];
   ratingActual = [];
   feedbackList=[];
   feedbackFormActualData = [];
    event_id = 123;
  constructor(private _fd: FetchDataService) { }

ngOnInit(){}
showModal = false;
toggleModal() {
  this.showModal = !this.showModal;

  this._fd.getRating(this.event_id).subscribe(res => {
    this.ratingListFed = res.result;
  console.log('ratingData', this.ratingListFed);
   
  })

  this._fd.getFeedback(this.event_id).subscribe(res=>{
    this.feedbackList=res.result;
    //console.log('check respons',this.feedbackList);
    
  })
}

// playAudio(){
//   let abc : any= document.getElementById('myAudio'); 
//   abc.play(); 
// }
  
closePopup(){
  $('.feebackModal').modal('hide');
}


radioValue(rateValue, radio){
  let rateingValue = rateValue
  let ratingID = radio
  let rateingData = {'rating_id':ratingID, 'rate':rateingValue}
  this.ratingActual.push(rateingData);

}


submitFeedback(){
  let userID: any = JSON.parse(localStorage.getItem('virtual'));
  let agenda = (<HTMLInputElement>document.getElementById("agenda")).value;
 let registration= (<HTMLInputElement>document.getElementById("registration")).value;
 let event= (<HTMLInputElement>document.getElementById("event")).value;


 let formDataNew:any = this.ratingActual

let data:any =[{'feedback_id':1, 'feedback':agenda}, {'feedback_id':2, 'feedback':registration},{'feedback_id':3, 'feedback':event}];

  const formData = new FormData();
  formData.append('rate', JSON.stringify(formDataNew));
  formData.append('user_id', userID.id);
 console.log('rating data', formData)
  this._fd.postRating(formData).subscribe((res: any) => {
    if(res.code===1){
      $('.feebackModal').modal('hide');
    }
  })

  const newFormData = new FormData()
newFormData.append('feedback', JSON.stringify(data));
newFormData.append('user_id', userID.id);

console.log('feedback check', newFormData);

  this._fd.postFeedback(newFormData).subscribe((res: any) => {
    if(res.code===1){
      $('.feebackModal').modal('hide');
    }
  })
  
  

}
}