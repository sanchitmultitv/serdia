import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuditoriumComponent } from './auditorium.component';
import { LeftAuditoriumComponent } from './components/left-auditorium/left-auditorium.component';
import { RightAuditoriumComponent } from './components/right-auditorium/right-auditorium.component';
import { componentFactoryName } from '@angular/compiler';
import { FrontAuditoriumDeskComponent } from './components/front-auditorium-desk/front-auditorium-desk.component';
import { FrontAuditoriumComponent } from './components/front-auditorium/front-auditorium.component';


const routes: Routes = [
  {path:'',
  component: AuditoriumComponent,
  children:[
    {path:'', redirectTo:'left'},
    {path:'left', component:LeftAuditoriumComponent},
    {path:'right', component:RightAuditoriumComponent},
    {path:'front-desk', component:FrontAuditoriumDeskComponent},
    {path: 'front-auditorium', component:FrontAuditoriumComponent}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuditoriumRoutingModule { }
