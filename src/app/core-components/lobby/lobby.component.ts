import { Component, OnInit, ViewChild, ElementRef, OnDestroy, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
declare var introJs: any;
@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.scss']
})
export class LobbyComponent implements OnInit, OnDestroy, AfterViewInit {
  videoEnd = false;
  receptionEnd = false;
  showVideo = false;
  auditoriumLeft = false;
  auditoriumRight = false;
  exhibitionHall = false;
  registrationDesk = false;
  networkingLounge = false;
  intro: any;

  @ViewChild('recepVideo', { static: true }) recepVideo: ElementRef;

  constructor(private router: Router) { }

  ngOnInit(): void {
    if (localStorage.getItem('user_guide') === 'start') {
      this.getUserguide();
    }
  }
  ngAfterViewInit() {
    this.playAudio();
  }



  playAudio() {
    localStorage.setItem('play', 'play');
    let abc: any = document.getElementById('myAudio');
    abc.play();
    alert('after login audio')
  }
  playevent() {
    this.videoEnd = true;
  }
  playReception() {
    // this.receptionEnd = true;
    // let vid: any = document.getElementById('recepVideo');
    // console.log('ppp', vid)
    // // vid.play()
    // this.recepVideo.nativeElement.play();
    this.router.navigate(['/welcome']);

  }
  receptionEndVideo() {
    this.receptionEnd = false;
    this.router.navigate(['/stage'])
  }

  gotoAuditoriumFront(){
    this.router.navigate(['/auditorium/front-desk']);
  }

  playAuditoriumLeft() {
    this.auditoriumLeft = true;
    this.showVideo = true;
    let vid: any = document.getElementById("audLeftvideo");
    vid.play();
  }
  gotoAuditoriumLeftOnVideoEnd() {
    this.router.navigate(['/auditorium/left']);
  }
  playAuditoriumRight() {
    this.auditoriumRight = true;
    this.showVideo = true;
    let vid: any = document.getElementById("audRightvideo");
    vid.play();
  }
  gotoAuditoriumRightOnVideoEnd() {
    this.router.navigate(['/auditorium/right']);
  }
  playExhibitionHall() {
    this.exhibitionHall = true;
    this.showVideo = true;
    let vid: any = document.getElementById("exhibitvideo");
    vid.play();
  }
  gotoExhibitionHallOnVideoEnd() {
    this.router.navigate(['/exhibitionHall']);
  }
  playRegistrationDesk() {
    this.registrationDesk = true;
    this.showVideo = true;
    let vid: any = document.getElementById("regDeskvideo");
    vid.play();
  }
  gotoRegistrationDeskOnVideoEnd() {
    this.router.navigate(['/registrationDesk']);
  }
  playNetworkingLounge() {
    this.networkingLounge = true;
    this.showVideo = true;
    let vid: any = document.getElementById("netLoungevideo");
    vid.play();
  }
  gotoNetworkingLoungeOnVideoEnd() {
    this.router.navigate(['/networkingLounge']);
  }


  getUserguide() {
    this.intro = introJs().setOptions({
      hidePrev: true,
      hideNext: true,
      exitOnOverlayClick: false,
      exitOnEsc: false,
      steps: [
        {
          element: document.querySelector("#reception_pulse"),
          intro: "<div style='text-align:center'>Welcome to the Event</div>"
        },
        {
          element: document.querySelectorAll("#heighlight_pulse")[0],
          intro: "<div style='text-align:center'>This is the Highlights of the Event can be viewed</div>"
        },
        {
          element: document.querySelectorAll("#agenda_pulse")[0],
          intro: "<div style='text-align:center'>Agenda can be viewed</div>"
        },
        {
          element: document.querySelectorAll("#networking_pulse")[0],
          intro: "<div style='text-align:center'>We have some interesting activities lined up for you at networking lounge, so do visit!</div>"
        },
        {
          element: document.querySelectorAll("#audRight_pulse")[0],
          intro: "<div style='text-align:center'>All the keynote presentations will be held at the Grand Ballroom</div>"
        },
        {
          element: document.querySelector("#frontaudi_pulse"),
          intro: "<div style='text-align:center'>This is the Audi 3 & Audi 4</div>"
        },
      ]
    }).oncomplete(() => document.cookie = "intro-complete=true");

    let start = () => this.intro.start();
    start();
    // if (document.cookie.split(";").indexOf("intro-complete=true") < 0)
    //   window.setTimeout(start, 1000);
  }
  ngOnDestroy() {
    if (localStorage.getItem('user_guide') === 'start') {
      let stop = () => this.intro.exit();
      stop();
    }
    localStorage.removeItem('user_guide');
  }
}
