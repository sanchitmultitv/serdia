import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ChatService } from 'src/app/services/chat.service';
declare var $: any;
@Component({
  selector: 'app-networking-lounge',
  templateUrl: './networking-lounge.component.html',
  styleUrls: ['./networking-lounge.component.scss']
})
export class NetworkingLoungeComponent implements OnInit {
  videoEnd=false;
  liveMsg= false;
  videoPlayer = '../assets/video/networking_lounge_video.mp4';
  constructor(private router: Router, private chat: ChatService) { }

  ngOnInit(): void {
    this.chat.getconnect('toujeo-123');
    this.chat.getMessages().subscribe((data=>{
      console.log('data',data);
      if(data == 'start_live'){
        this.liveMsg = true;
      }
      if(data == 'stop_live'){
        this.liveMsg = false;
      }
    }))
  }
  videoEnded() {
    this.videoEnd = true;
  }
  openWtsapp(){
    $('.wtsappModal').modal('show');
  }
  openChat(){
    $('.chatsModal').modal('show');
  }
  openCamera(){
    this.router.navigate(['/capturePhoto']);
  }

}
