import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { WebcamImage, WebcamInitError, WebcamUtil } from 'ngx-webcam';
declare var introJs: any;
declare var Webcam: any;

@Component({
  selector: 'app-stage',
  templateUrl: './stage.component.html',
  styleUrls: ['./stage.component.scss']
})
export class StageComponent implements OnInit {
  img;
  constructor() { }

  // ngOnInit(): void {
  //   Webcam.set({
  //     width: 320,
  //     height: 240,
  //     image_format: 'jpeg',
  //     jpeg_quality: 90
  //   });
  //   Webcam.attach('#my_camera');









  //   var intro = introJs().setOptions({
  //     hidePrev: true,
  //     hideNext: true,
  //     exitOnOverlayClick: false,
  //     exitOnEsc: false,
  //     steps: [
  //       {
  //         element: document.querySelector("#content"),
  //         intro: "This whole area does something."
  //       },
  //       {
  //         element: document.querySelectorAll("#liFirst")[0],
  //         intro: "This is the first item in the list. It also does something."
  //       },
  //       {
  //         element: document.querySelectorAll("#liSec")[0],
  //         intro: "This second item probably does something as well"
  //       },
  //       {
  //         element: document.querySelectorAll("#button")[0],
  //         intro:
  //           "<p>This is a button that needs a good clicking</p>" +
  //           '<video src="https://media.giphy.com/media/l41YbDNR4q8BZJ7tm/giphy.mp4" autoplay loop />',
  //         tooltipClass: "wideo"
  //       }
  //     ]
  //   }).oncomplete(() => document.cookie = "intro-complete=true");

  //   var start = () => intro.start();

  //   window.addEventListener("load", () =>
  //     document.querySelector("#starter").addEventListener("click", e => {
  //       e.preventDefault();
  //       start();
  //     })
  //   );

  //   if (document.cookie.split(";").indexOf("intro-complete=true") < 0)
  //     window.setTimeout(start, 1000);

  // }

  take_snapshot() {

    // take snapshot and get image data
    Webcam.snap((data_uri) => {
      this.img = data_uri;
      console.log('data', data_uri)
    });
  }


    // toggle webcam on/off
    public showWebcam = true;
    public allowCameraSwitch = true;
    public multipleWebcamsAvailable = false;
    public deviceId: string;
    public videoOptions: MediaTrackConstraints = {
      // width: {ideal: 1024},
      // height: {ideal: 576}
    };
    public errors: WebcamInitError[] = [];
  
    // latest snapshot
    public webcamImage: WebcamImage = null;
  
    // webcam snapshot trigger
    private trigger: Subject<void> = new Subject<void>();
    // switch to next / previous / specific webcam; true/false: forward/backwards, string: deviceId
    private nextWebcam: Subject<boolean|string> = new Subject<boolean|string>();
  
    public ngOnInit(): void {
      WebcamUtil.getAvailableVideoInputs()
        .then((mediaDevices: MediaDeviceInfo[]) => {
          this.multipleWebcamsAvailable = mediaDevices && mediaDevices.length > 1;
        });
    }
  
    public triggerSnapshot(): void {
      this.trigger.next();
    }
  
    public toggleWebcam(): void {
      this.showWebcam = !this.showWebcam;
    }
  
    public handleInitError(error: WebcamInitError): void {
      this.errors.push(error);
    }
  
    public showNextWebcam(directionOrDeviceId: boolean|string): void {
      // true => move forward through devices
      // false => move backwards through devices
      // string => move to device with given deviceId
      this.nextWebcam.next(directionOrDeviceId);
    }
  
    public handleImage(webcamImage: WebcamImage): void {
      console.info('received webcam image', webcamImage);
      this.webcamImage = webcamImage;
    }
  
    public cameraWasSwitched(deviceId: string): void {
      console.log('active device: ' + deviceId);
      this.deviceId = deviceId;
    }
  
    public get triggerObservable(): Observable<void> {
      return this.trigger.asObservable();
    }
  
    public get nextWebcamObservable(): Observable<boolean|string> {
      return this.nextWebcam.asObservable();
    }
}
