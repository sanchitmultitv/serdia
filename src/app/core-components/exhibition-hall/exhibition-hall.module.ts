import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExhibitionHallRoutingModule } from './exhibition-hall-routing.module';
import { ExhibitLifeComponent } from './exhibit-life/exhibit-life.component';
import { LifeboyComponent } from './lifeboy/lifeboy.component';
import { VgCoreModule } from 'videogular2/compiled/core';
import { VgControlsModule } from 'videogular2/compiled/controls';
import { VgOverlayPlayModule } from 'videogular2/compiled/overlay-play';
import { VgBufferingModule} from 'videogular2/compiled/buffering';



@NgModule({
  declarations: [ExhibitLifeComponent, LifeboyComponent],
  imports: [
    CommonModule,
    ExhibitionHallRoutingModule,
    VgCoreModule,
    VgControlsModule,
    VgBufferingModule,
    VgOverlayPlayModule,
  ]
})
export class ExhibitionHallModule { }
