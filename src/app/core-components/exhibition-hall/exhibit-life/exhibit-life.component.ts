import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-exhibit-life',
  templateUrl: './exhibit-life.component.html',
  styleUrls: ['./exhibit-life.component.scss']
})
export class ExhibitLifeComponent implements OnInit {

  constructor( private router: Router) { }

  ngOnInit(): void {
  }
  lifeboy(){
this.router.navigate(['/exhibitionHall/lifeboy']);
  }
}
