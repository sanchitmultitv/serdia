import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExhibitionHallComponent } from './exhibition-hall.component';
import { ExhibitLifeComponent } from './exhibit-life/exhibit-life.component';
import { LifeboyComponent } from './lifeboy/lifeboy.component';


const routes: Routes = [
  {path:'', component:ExhibitionHallComponent,
  children:[
    {path:'', redirectTo:'life' },
    {path:'life', component:ExhibitLifeComponent},
    {path:'lifeboy', component:LifeboyComponent}
  ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExhibitionHallRoutingModule { }
