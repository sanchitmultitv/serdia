import { Component, OnInit } from '@angular/core';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import {FormGroup,FormControl} from '@angular/forms';
declare var $: any;
@Component({
  selector: 'app-poll',
  templateUrl: './poll.component.html',
  styleUrls: ['./poll.component.scss']
})
export class PollComponent implements OnInit {
pollingLIst:any =[];
pollForm = new FormGroup({
  polling: new FormControl(''),
});
msg;
  constructor(private _fd : FetchDataService) { }

  ngOnInit(): void {
    this.getPolls();
  }
  closePopup() {
    $('.pollModal').modal('hide');
  }
getPolls() {
  this._fd.getPollList().subscribe(res=>{
    this.pollingLIst = res.result;
  })
}
pollSubmit(id){
  let data = JSON.parse(localStorage.getItem('virtual'));
  // console.log(id);
  // console.log(data.id);
  this._fd.postPoll(id,data.id,this.pollForm.value.polling).subscribe(res=>{
    console.log(res);
    if(res.code == 1){
      this.msg = 'Submitted Succesfully';
    }
  })
}

}
