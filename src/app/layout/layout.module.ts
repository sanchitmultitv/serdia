import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { ProfileComponent } from './profile/profile.component';
import { AttendeesComponent } from './attendees/attendees.component';
import { AgendaComponent } from './agenda/agenda.component';
import { ChatsComponent } from './chats/chats.component';
import { MyContactsComponent } from './my-contacts/my-contacts.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MyFeedbackComponent } from './my-feedback/my-feedback.component';
import { KbcComponent } from './kbc/kbc.component';
import { CapturePhotoComponent } from './capture-photo/capture-photo.component';
import { QuestionComponent } from './question/question.component';
import { PollComponent } from './poll/poll.component';
import {WtsppFileComponent} from './wtspp-file/wtspp-file.component';
import { HeightlightComponent } from './heightlight/heightlight.component';
import { AudioScreenComponent } from './audio-screen/audio-screen.component';
import { BallroomQuestionComponent } from './ballroom-question/ballroom-question.component';
import { GroupChatComponent } from './group-chat/group-chat.component';
import { DocsInfoComponent } from './docs-info/docs-info.component';
import { SalesInfoComponent } from './sales-info/sales-info.component';
import { ScheduleCallComponent } from './schedule-call/schedule-call.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { BriefcaseDocsComponent } from './briefcase-docs/briefcase-docs.component';



@NgModule({
  declarations: [
    LayoutComponent,
    ProfileComponent,
    AttendeesComponent,
    AgendaComponent,
    ChatsComponent,
    MyContactsComponent,
    KbcComponent,
    CapturePhotoComponent,
    QuestionComponent,
    PollComponent,
    WtsppFileComponent,
    MyFeedbackComponent, HeightlightComponent, AudioScreenComponent, BallroomQuestionComponent, GroupChatComponent, DocsInfoComponent, SalesInfoComponent, ScheduleCallComponent, BriefcaseDocsComponent],
exports:[MyFeedbackComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    LayoutRoutingModule,
    NgbModule

  ]
})
export class LayoutModule { }
