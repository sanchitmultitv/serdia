import { Component, OnInit, ElementRef, Renderer2, ViewChild, Output, EventEmitter } from '@angular/core';
import { FetchDataService } from '../../services/fetch-data.service';
import { toBase64String } from '@angular/compiler/src/output/source_map';
declare var $: any;

@Component({
  selector: 'app-capture-photo',
  templateUrl: './capture-photo.component.html',
  styleUrls: ['./capture-photo.component.scss']
})
export class CapturePhotoComponent implements OnInit {
  videoWidth = 0;
  videoHeight = 0;
  showImage = false;
  constructor(private renderer: Renderer2, private _fd: FetchDataService) { }
  @ViewChild('video', { static: true }) videoElement: ElementRef;
  @ViewChild('canvas', { static: true }) canvas: ElementRef;
  @Output('myOutputVal') myOutputVal = new EventEmitter();

  ngOnInit(): void {

    // this.startCamera();
    $('.videoData').show();
  }

  openCapturePhotoModal() {
    $('.capturePhotoModal').modal('show');
    this.startCamera();
  }
  constraints = {
    video: {
      facingMode: "environment",
      width: { ideal: 720 },
      height: { ideal: 480 }
    }
  };

  startCamera() {
    if (!!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia)) {
      navigator.mediaDevices.getUserMedia(this.constraints).then(this.attachVideo.bind(this)).catch(this.handleError);
    } else {
      alert('Sorry, camera not available.');
    }

  }
  handleError(error) {
    console.log('Error: ', error);
  }
  stream: any;
  attachVideo(stream) {
    this.renderer.setProperty(this.videoElement.nativeElement, 'srcObject', stream);
    this.renderer.listen(this.videoElement.nativeElement, 'play', (event) => {
      this.videoHeight = this.videoElement.nativeElement.videoHeight;
      this.videoWidth = this.videoElement.nativeElement.videoWidth;
    });
  }
  img;
  dataurl;
  capture() {
    this.showImage = true;
    this.renderer.setProperty(this.canvas.nativeElement, 'width', this.videoWidth);
    this.renderer.setProperty(this.canvas.nativeElement, 'height', this.videoHeight);
    this.canvas.nativeElement.getContext('2d').drawImage(this.videoElement.nativeElement, 0, 0);
    let canvas: any = document.getElementById("canvas");
    let context = canvas.getContext('2d');
    context.strokeStyle = "#ffc90e";
    context.lineWidth = 2;
    let vid: any = document.getElementById("vid");
    // var logo = document.getElementById("logo");
    // context.drawImage(logo, 5, 5, 150, 70);
    // context.strokeRect(0, 0, canvas.width, canvas.height);
    // let imgPath = '../../../assets/img/xtrem.jpeg';
    // let imgPath = '../../../assets/img/watermark.png';
    // let imgObj = new Image();
    // imgObj.src = imgPath;
    let watermark = new Image();
    context.beginPath();
    context.lineWidth = 4;
    context.strokeStyle = "#fff";
    context.fillStyle = "#FFFFFF";
    let btmRect = vid.videoHeight - 50;
    context.fillRect(0, btmRect, vid.videoWidth, 50);
    context.textAlign = 'left';
    context.fillStyle = 'black';
    context.font = '14pt sans-serif';
    context.textBaselinke = 'middle';
    let textString = 'Powered To Do More';
    let textWidth = context.measureText(textString).width;
    context.fillText(textString, (canvas.width / 2 - 90), (canvas.height - 15), 360);
    context.stroke();
    context.beginPath();
    watermark.src = '../../../assets/img/watermark.png';
    context.drawImage(watermark, 0, (btmRect));
    this.dataurl = canvas.toDataURL('mime');
    this.img = canvas.toDataURL("image/png");

    // console.log('kkkk', imgObj.src)
    // $('.videoData').hide();
  }
  
  uploadVideo() {
    let user_id = JSON.parse(localStorage.getItem('virtual')).id;
    let user_name = JSON.parse(localStorage.getItem('virtual')).name;
    const formData = new FormData();
    formData.append('user_id', user_id);
    formData.append('user_name', user_name);
    formData.append('image', this.img);
    this._fd.uploadsample(formData).subscribe(res => {
      console.log('upload', res)
    });
  }

  showCapture() {
    $('.capturePhoto').modal('show');
    this.startCamera();
  }

  closeCamera() {
    location.reload();
    $('.capturePhotoModal').modal('hide');

  }
  reload() {
    this.showImage = false;
    this.startCamera();
  }
}
