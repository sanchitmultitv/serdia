import { Component, OnInit, Renderer2, ViewChild, ElementRef, Output } from '@angular/core';
import { fadeAnimation } from '../shared/animation/fade.animation';
import { Router } from '@angular/router';
import { EventEmitter } from 'events';
import { AuthService } from '../services/auth.service';
declare var $: any;
@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
  animations: [fadeAnimation]

})
export class LayoutComponent implements OnInit {
  firstName;
  constructor(public router: Router, private renderer: Renderer2, private auth: AuthService) { }

  ngOnInit(): void {
    $('.videoData').show();
    this.firstName = JSON.parse(localStorage.getItem('virtual')).name;
    this.playAudio();
  }
  public getRouterOutletState(outlet) {
    return outlet.isActivated ? outlet.activatedRoute : '';
  }
  showModal = false;
  toggleModal() {
    this.showModal = !this.showModal;
  }

  readOutputValueEmitted(event) {
    console.log('kkkk', event)
  }

  videoWidth = 0;
  videoHeight = 0;
  showImage = false;
  @ViewChild('video', { static: true }) videoElement: ElementRef;
  @ViewChild('canvas', { static: true }) canvas: ElementRef;
  @Output('myOutputVal') myOutputVal = new EventEmitter();

  playAudio(){
    let abc : any= document.getElementById('myAudio');
    abc.play();
  }
  constraints = {
    video: {
      facingMode: "environment",
      width: { ideal: 720 },
      height: { ideal: 480 }
    }
  };

  startCamera() {
    if (!!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia)) {
      navigator.mediaDevices.getUserMedia(this.constraints).then(this.attachVideo.bind(this)).catch(this.handleError);
    } else {
      alert('Sorry, camera not available.');
    }

  }
  handleError(error) {
    console.log('Error: ', error);
  }
  stream: any;
  attachVideo(stream) {
    this.renderer.setProperty(this.videoElement.nativeElement, 'srcObject', stream);
    this.renderer.listen(this.videoElement.nativeElement, 'play', (event) => {
      this.videoHeight = this.videoElement.nativeElement.videoHeight;
      this.videoWidth = this.videoElement.nativeElement.videoWidth;
    });
  }
  img;
  capture() {
    this.showImage = true;
    this.renderer.setProperty(this.canvas.nativeElement, 'width', this.videoWidth);
    this.renderer.setProperty(this.canvas.nativeElement, 'height', this.videoHeight);
    this.canvas.nativeElement.getContext('2d').drawImage(this.videoElement.nativeElement, 0, 0);
    let canvas: any = document.getElementById("canvas");
    let context = canvas.getContext('2d');
    context.strokeStyle = "#ffc90e";
    context.lineWidth = 5;
    var img = document.getElementById("logo");
    context.drawImage(img, 5, 5, 120, 50);
    context.strokeRect(0, 0, canvas.width, canvas.height);
    let imgPath = '../../../assets/img/xtrem.jpeg';
    let imgObj = new Image();

    imgObj.src = imgPath;
    this.img = canvas.toDataURL("image/png");
    console.log('kkkk', imgObj.src)
    // $('.videoData').hide();
  }
  stopVideo() {
    this.renderer.listen(this.videoElement.nativeElement, 'stop', (event) => {
      console.log('dsfj')
    });
  }

  showCapture() {
    $('.capturePhoto').modal('show');
    this.startCamera();
  }

  closeCamera() {
    location.reload();
    $('.capturePhoto').modal('hide');

  }

  playAudioClap(){
    let playaudio : any= document.getElementById('myAudioClap');
    playaudio.play();
  }

  logout(){
    let user_id = JSON.parse(localStorage.getItem('virtual')).id;
    this.auth.logout(user_id).subscribe(res=>{
      this.router.navigate(['/login']);
    });
  }
}
