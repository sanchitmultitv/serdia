import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ChatService } from 'src/app/services/chat.service';
import { FetchDataService } from 'src/app/services/fetch-data.service';
declare var $: any;
@Component({
  selector: 'app-group-chat',
  templateUrl: './group-chat.component.html',
  styleUrls: ['./group-chat.component.scss']
})
export class GroupChatComponent implements OnInit {
  textMessage = new FormControl('');
  newMessage: string[] = [];
  msgs: string;
  messageList: any=[];
  roomName= 'virtuals';
  constructor(private chatService : ChatService, private _fd : FetchDataService) { }

  ngOnInit(): void {
    this.chatGroup();
    let data = JSON.parse(localStorage.getItem('virtual'));
    this.chatService.addUser(data.name, this.roomName);
    localStorage.setItem('username', data.name);
    this.chatService
      .receiveMessages()
      .subscribe((msgs: string) => {
    this.messageList.push(msgs);
        console.log('demo',this.messageList);
      });
  }
  chatGroup(){
    this._fd.groupchating().subscribe(res=>{
      console.log('res',res);
      this.messageList = res.result;
    });
  }
  closePopup(){
    $('.groupchatsModal').modal('hide');
  }
  postMessage(value){
   // console.log(value);
    let data = JSON.parse(localStorage.getItem('virtual'));
   // console.log(data.name);

      this.chatService.sendMessage(value, data.name, this.roomName);
      this.textMessage.reset();
      this.chatGroup();
      //this.newMessage.push(this.msgs);


  }
}
