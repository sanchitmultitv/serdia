import { Component, OnInit } from '@angular/core';
import { FetchDataService } from 'src/app/services/fetch-data.service';
declare var $: any;
@Component({
  selector: 'app-wtspp-file',
  templateUrl: './wtspp-file.component.html',
  styleUrls: ['./wtspp-file.component.scss']
})
export class WtsppFileComponent implements OnInit {
wtspp:any=[];
  constructor(private _fd: FetchDataService) { }

  ngOnInit(): void {

  }
  closePopup(){
    $('.wtsappModal').modal('hide');
  }
}
