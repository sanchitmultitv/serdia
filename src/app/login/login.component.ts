import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

import { Router } from '@angular/router';
import { FetchDataService } from '../services/fetch-data.service';
import { AuthService } from '../services/auth.service';
function emailDomainValidator(control: FormControl) {
  let email = control.value;
  if (email && email.indexOf("@") != -1) {
    let [_, domain] = email.split("@");
    if (domain !== "sanofi.com" && domain !== "sanofi-india.com") {
      return {
        emailDomain: {
          parsedDomain: domain
        }
      }
    }
  }
  return null;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  token;
  msg;
  loginForm:FormGroup
  coverImage = "../../assets/img/h-about.jpg";
  videoPlay = false;
  constructor(private router: Router, private _fd: FetchDataService, private auth: AuthService, private formBuilder:FormBuilder) { }

  ngOnInit(): void {
    localStorage.setItem('user_guide', 'start');

    this.loginForm = this.formBuilder.group({
      email: ['',[Validators.email,
       Validators.pattern("[^ @]*@[^ @]*"),
          emailDomainValidator]]

    });



  }

  loggedIn() {
    const user = {
      email: this.loginForm.get('email').value,
     // password: this.loginForm.get('password').value,
      event_id: 123,
      role_id: 1
    };
   // console.log("password",user.password)
    this.auth.loginMethod(user).subscribe((res: any) => {
      if (res.code === 1) {
        this.videoPlay = true;
        localStorage.setItem('virtual', JSON.stringify(res.result));
        this.videoPlay = true;
        let vid: any = document.getElementById('myVideo');
        vid.play();
      } else {
        this.msg = 'Invalid Login';
        this.videoPlay = false;
        this.loginForm.reset();
      }
    }, (err: any) => {
      this.videoPlay = false;
      console.log('error', err)
    });
    // this._fd.authLogin(user).subscribe(res => {
    //   if (res.code === 1) {
    //     this.videoPlay = true;
    //     localStorage.setItem('virtual', JSON.stringify(res.data));
    //     // this.router.navigate(['/lobby']);
    //     this.videoPlay = true;
    //     let vid: any = document.getElementById('myVideo');
    //     vid.play();
    //   } else {
    //     this.msg = 'Invalid Login';
    //     this.videoPlay = false;
    //     this.loginForm.reset();
    //   }
    // }, err => {
    //   this.videoPlay = false;
    //   console.log('error', err)
    // });
  }

  endVideo() {
    this.router.navigateByUrl('/lobby');
    let welcomeAuido:any = document.getElementById('myAudio');
    welcomeAuido.play();
  }
}
