export const ApiConstants = {
    signup:'virtualapi/v1/auth/signup/event_id',
    login: 'virtualapi/v1/auth/login',
    acmeLogin: 'virtualapi/v1/acma/attendees/login',
    // login: 'virtualapi/v1/auth/login/event_id/123/email/w@w.com/password/qwer/role_id/1',
    profile: 'virtualapi/v1/auth/profile/get',
    attendees: 'virtualapi/v1/get/attendees/event_id',
    addComment: 'virtualapi/v1/user/comment/add',
    chatAttendeesComments: 'virtualapi/v1/get/attendees',
    // one2oneCommentPost: 'virtualapi/v1/one2one/chat/request',
    one2oneCommentPost: 'virtualapi/v1/one2one/chat/post',
    One2OneCommentList: 'virtualapi/v1/one2one/chat/get',
    commentsList: 'virtualapi/v1/user/comment/grouplist',
    ratingMaster:'virtualapi/v1/rating/master/list',
    feedbackMaster:'virtualapi/v1/feedback/master/list',
    ratingPost: 'virtualapi/v1/rating/post',
    feebackPost:'virtualapi/v1/feedback/post',
    commentsLists: 'virtualapi/v1/user/comment/grouplist',
    askQuestion: 'virtualapi/v1/ask/question/post',
    askQuestionLive: 'virtualapi/v1/ask/live/question/post',
    getPolls: 'virtualapi/v1/get/poll/event_id/123',
    postPolls: 'virtualapi/v1/poll/answer/post',
    getQuestionAnswser: 'virtualapi/v1/asked/question/answer/get/user_id',
    getliveAnswser: 'virtualapi/v1/get/live/questions/event_id/123',
    wtsappAgendaFiles:'virtualapi/v1/get/document/event_id/123',
    quizlist: 'virtualapi/v1/get/quiz',
    submitQuiz: 'virtualapi/v1/quiz/answer/post',
    summaryQuiz: 'virtualapi/v1/get/quiz/summary',
    uploadPic: 'virtualapi/v1/upload/pic',
    logout: 'virtualapi/v1/auth/attendee/logout',
    groupChats: 'virtualapi/v1/get/mongo/groupchat/room/virtuals',
    briefcaseList: 'virtualapi/v1/get/briefcase/list',
    postBriefcase: 'virtualapi/v1/addto/briefcase/post',
    exhibition:'virtualapi/v1/get/exhibitions/event_id/123'

}
