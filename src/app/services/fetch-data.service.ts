import { Injectable } from '@angular/core';
import { Login } from './module/login';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpParams, HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { ApiConstants } from './apiConfig/api.constants';

@Injectable({
  providedIn: 'root'
})
export class FetchDataService {
  baseUrl = environment.baseUrl;
  loginBaseUrl = environment.url;
  login = 'RecognizerApi/index.php/api/engine/authentication';


  constructor(private http: HttpClient) { }
  authLogin(user: Login): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    let params = new HttpParams();
    params = params.set('email', user.email);
    params = params.set('password', user.password);
    return this.http.post(`${this.loginBaseUrl}/${this.login}`, params);
  }

  getProfile(id, role_id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.profile}/id/${id}/role_id/${role_id}`);
  }
  getAttendees(event_id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.attendees}/${event_id}`);
  }
  getAttendeesbyName(event_id,name): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.attendees}/${event_id}/name/${name}`);
  }
  getComments(event_id, user_id, typ): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.commentsList}/event_id/${event_id}/type/${typ}`);
  }
  postComments(comment: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/${ApiConstants.addComment}`, comment);
  }
  getOne2oneChatList(event_id, name): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.chatAttendeesComments}/event_id/${event_id}/name/${name}`);
  }
  enterTochatList(receiver_id, sender_id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.One2OneCommentList}/receiver_id/${receiver_id}/sender_id/${sender_id}`)
  }
  postOne2oneChat(comment: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/${ApiConstants.one2oneCommentPost}`, comment);
  }
  getRating(event_id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.ratingMaster}/event_id/${event_id}`);
  }
  getFeedback(event_id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.feedbackMaster}/event_id/${event_id}`);
  }
  postFeedback(feedback: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/virtualapi/v1/feedback/post`, feedback);
  }
  postRating(rating: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/virtualapi/v1/rating/post`, rating);
  }
  getExhibition():Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.exhibition}`);
  }
  getQuiz() {
    return this.http.get('https://opentdb.com/api.php?amount=50&category=11&type=multiple');
  }

  askQuestions(id, value): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    let params = new HttpParams();
    params = params.set('user_id', id);
    params = params.set('question', value);
    return this.http.post(`${this.baseUrl}/${ApiConstants.askQuestion}`, params);
  }
  askLiveQuestions(id, value): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    let params = new HttpParams();
    params = params.set('user_id', id);
    params = params.set('question', value);
    return this.http.post(`${this.baseUrl}/${ApiConstants.askQuestionLive}`, params);
  }
  getanswers(uid): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.getQuestionAnswser}/${uid}/event_id/123`)
  }
  Liveanswers(): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.getliveAnswser}`)
  }
  getPollList(): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.getPolls}`);
  }
  postPoll(id, uid, value): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    let params = new HttpParams();
    params = params.set('poll_id', id);
    params = params.set('user_id', uid);
    params = params.set('answer', value);
    return this.http.post(`${this.baseUrl}/${ApiConstants.postPolls}`, params);
  }
  getWtsappFiles(): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.wtsappAgendaFiles}`);
  }

  getQuizList(event_id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.quizlist}/event_id/${event_id}`);
  }
  postSubmitQuiz(quiz): Observable<any> {
    return this.http.post(`${this.baseUrl}/${ApiConstants.submitQuiz}`, quiz);
  }
  getSummaryQuiz(event_id, user_id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.summaryQuiz}/event_id/${event_id}/user_id/${user_id}`);
  }

  uploadCameraPic(pic): Observable<any> {
    return this.http.post(`${this.baseUrl}/${ApiConstants.uploadPic}`, pic);
  }
  uploadsample(pic): Observable<any>{
    return this.http.post(`https://tbbmedialive.com/Q3PCM2020/uploadblob.php`, pic);
  }
  groupchating(): Observable<any>{
    return this.http.get(`${this.baseUrl}/${ApiConstants.groupChats}`)
  }
  submitWheelScore(wheel:any){
    return this.http.post(`https://tbbmedialive.com:7000/virtualapi/v1/user/game/post`, wheel);
  }
  getBriefcaseList(event_id, user_id){
    return this.http.get(`${this.baseUrl}:7000/${ApiConstants.briefcaseList}/event_id/${event_id}/user_id/${user_id}`);
  }
  postBriefcase(doc:any){
    return this.http.post(`${this.baseUrl}:7000/${ApiConstants.postBriefcase}`, doc);
  }
  analyticsPost(analytics: any): Observable<any>{
    return this.http.post(`https://tbbmedialive.com:7000/virtualapi/v1/analytics/user/history/add`, analytics);
  }
}
